import { resolver } from 'graphql-sequelize';
import { Message } from '../../models';

import to from 'await-to-js';

export const MessageMap = {
  channel: resolver(Message.associations.channel)
};
