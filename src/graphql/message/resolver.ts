import { Query } from './message.query';
import { MessageMap } from "./message.map";
import { Mutation } from "./message.mutation";

import { GraphQLScalarType } from 'graphql';
import { Kind } from 'graphql/language';

export const resolver = {
  Query: Query,
  Message: MessageMap,
  Mutation: Mutation,	
  Date: new GraphQLScalarType({
    name: 'Date',
    description: 'Date custom scalar type',
    parseValue(value) {
      return new Date(value); // value from the client
    },
    serialize(value) {
      return value.getTime(); // value sent to the client
    },
    parseLiteral(ast) {
      if (ast.kind === Kind.INT) {
        return parseInt(ast.value, 10); // ast value is always in string format
      }
      return null;
    },
  }),
};
