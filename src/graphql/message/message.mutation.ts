import { resolver as rs } from 'graphql-sequelize';
import { Channel } from '../../models';
import { Message } from '../../models';
import { User } from '../../models';
import to from 'await-to-js';


export const Mutation = {

  createMessage: rs(Message, {
    before: async (findOptions, { data }, {user}) => {

      if(!user){
        throw new Error('User not authenticated');
      }

      let u;
      // user should be the part of channel
      let ch = await Channel.findOne({where: {id: data.channelId}, include: [User]});
      if(ch && ch.users){
        ch.users.forEach((ele) => {
          if(ele.id == user.id){
            u = true;
          }
        })
      }

      if(!u){
        throw new Error('User not associated with the provided channel');
      }

      data.userId = user.id;
      // let buf  = new Buffer(data.message.length);
      // await buf.write(data.message, 0);
      // data.message = buf;

      let err, message;
      [err, message] = await to(Message.create(data));
      if (err) {
        throw err;
      }
      findOptions.where = { id:message.id };
      return findOptions
    }
  }),

};
