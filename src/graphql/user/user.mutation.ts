import { resolver as rs } from 'graphql-sequelize';
import { User } from '../../models';
import to from 'await-to-js';

export const Mutation = {
    createUser: rs(User, {
      before: async (findOptions, { data }) => {
        let err, user;
        [err, user] = await to(User.create(data) );
        if (err) {
          throw err;
        }
        findOptions.where = { id:user.id };
        return findOptions
      },
      after: (user) => {
        user.jwt = user.getJwt();
        user.sendActivation();
        return user;
      }
    }),

    updateUser: rs(User, {
      before: async (findOptions, { data }, {user}) => {

        if(!user){
          throw new Error('User not authenticated');
        }
        
        let err;
        [err, user] = await to(user.update(data) );
        if (err) {
          throw err;
        }
        findOptions.where = { id:user.id };
        return findOptions
      },
      after: (user) => {
        user.jwt = user.getJwt();
        return user;
      }
    }),

    verifyUser: rs(User, {
      before: async (findOptions, {email, token}) => {
        
        if(await User.verifyToken(email, token)) {
          findOptions.where = {email: email};
        }
        return findOptions;
      },

      after: async (user) => {

        if(user.isVerified){
          return {
            message: "Successfully verified"
          };
        }

        return {
            message: "Invalid Token"
        }

      }
    }),

    resetPassword: rs(User, {
      before: async (findOptions, {token, password}) => {
        
        findOptions.where = {resetToken: token};
        
        return findOptions;
      },

      after: async (user, {password}) => {
        
        let data = {
          password: password
        };

        let err;
        [err, user] = await to(user.update(data));

        if (err) {
          throw err;
        }  

        return {
          message: "Successfully Updated."
        }
      }
    }),

    deleteUser: rs(User, {
      before: (findOptions,{}, {user}) => {
        if(!user)
          throw new Error(" You must be logged in to delete your account")
        findOptions.where = {id: user.id}
        return findOptions
      },

      after: async (user) => {
        await user.destroy();

        return {
          message: "Removed you!"
        }
      }
    })

};
