import { resolver as rs } from 'graphql-sequelize';
import { Channel } from '../../models';
import { userChannel } from '../../models';
import to from 'await-to-js';


export const Mutation = {
  createChannel: rs(Channel, {
    before: async (findOptions, { data }, {user}) => {

      if(!user){
        throw new Error('User not authenticated or not verified');
      }

      let err, channel;
      [err, channel] = await to(Channel.create(data) );
      if (err) {
        throw err;
      }
      let uC = await to(userChannel.create({channelId: channel.id, userId: user.id, isOwner: 1}))
      findOptions.where = { id:channel.id };
      return findOptions
    }
  }),

  deleteChannel: rs(Channel, {
    before: async (findOptions, {where}, {user}) => {

      if(!user){
        throw new Error('User not authenticated or not verified');
      }

      let err, channel;
      [err, channel] = await to(Channel.destroy({where: where}) );
      if (err) {
        throw err;
      }
    },

    after: (obj) => {
      return { message: "deleted"}
    }
  }),
  inviteUser: rs(Channel, {
    before: async(findOptions, {where}, {user}) => {

      if(!user){
        throw new Error('User not authenticated or not verified');
      }

      let err, channel;
      [err, channel] = await to(userChannel.findOne({
          where: {
            channelId: where.id,
            userId: user.id
          }
      }))

      if(!channel.isOwner){
        throw new Error("You must be owner to invite anyone")
      }

      await userChannel.create({
        channelId: where.id,
        userId: where.userId,
        isOwner: false
      })

      findOptions.where = {id: where.id}
      return findOptions
    },
    after: async(obj) => {
      return {
        message: 'Invited User.'
      }
    }
  }),
  kickUser: rs(Channel, {
    before: async(findOptions, {where}, {user}) => {

      if(!user){
        throw new Error('User not authenticated or not verified');
      }

      let err, channel;
      [err, channel] = await to(userChannel.findOne({
          where: {
            channelId: where.id,
            userId: user.id
          }
      }))

      if(!channel.isOwner){
        throw new Error("You must be owner to kick anyone")
      }

      [err, channel] = await to(userChannel.destroy({
          where: {
            channelId: where.id,
            userId: where.userId,
            isOwner: false
          }
      }))

      findOptions.where = {id: where.id}
      return findOptions
    },
    after: async(obj) => {
      return {
        message: 'Kicked that user.'
      }
    }
  })
};
