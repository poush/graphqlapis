import { Query } from './channel.query';
import { ChannelMap } from "./channel.map";
import { Mutation } from "./channel.mutation";

export const resolver = {
  Query: Query,
  Channel: ChannelMap,
  Mutation: Mutation,
};
