import { resolver } from 'graphql-sequelize';
import { Channel } from '../../models';
import to from 'await-to-js';

console.log(Channel.associations)
export const ChannelMap = {
  users: resolver(Channel.associations.users),
  messages: resolver(Channel.associations.messages),
};
