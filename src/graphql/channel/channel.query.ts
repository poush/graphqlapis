import { resolver } from 'graphql-sequelize';
import { Channel } from '../../models';
import { userChannel } from '../../models'
import to from 'await-to-js';

export const Query = {
    getChannel: resolver(Channel, {
      before: async (findOptions, { where }, {user}) => {

        if(!user){
          throw new Error('User not authenticated');
        }

        let err, channel;
        [err, channel] = await to(userChannel.findAll({
          where: {
            channelId: where.id,
            userId: user.id
          }
        }))
        if( !channel ){
          throw new Error('No such channel associated with logged in user')
        }
        if (err) {
          throw err;
        }

        return findOptions
      }
    }),
    listChannels: resolver(Channel, {list:true})
};
