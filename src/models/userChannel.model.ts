import { Table, Column, Model, HasMany, Default, PrimaryKey, AutoIncrement, BelongsTo, ForeignKey } from 'sequelize-typescript';
import { User } from './user.model'
import { Channel } from './channel.model'
@Table({timestamps: true})


export class userChannel extends Model<userChannel> {

  @ForeignKey(() => Channel)
  @Column
  channelId: number;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @Default(0)
  @Column
  isOwner: boolean;
}

export default userChannel;
