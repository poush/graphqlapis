import { 
  Table, 
  Column, 
  Model,
  BelongsTo,
  BeforeSave,
  PrimaryKey,
  AutoIncrement,
  ForeignKey, 
} from 'sequelize-typescript';
import * as Sequelize from "sequelize";
import { Channel } from './channel.model'
import { User } from './user.model'
import { ValidTypes } from '../config'
import { Validator } from '../services/validator'


@Table({timestamps: true})
export class Message extends Model<Message> {

  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column(Sequelize.TEXT)
  message;

  @ForeignKey(() => Channel)
  @Column
  channelId: number;

  @BelongsTo(() => Channel)
  channel: Channel;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @BelongsTo(() => User)
  user: User;

  @Column
  type: string;

  @BeforeSave
  static checkType(message: Message) {
    if(ValidTypes(message.type)){
      let valid, error;

      [valid, error] = Validator[message.type](message.message);

      if(valid)
        return true;

      else 
        throw new Error(error);

    }

    throw new Error("Invalid Message Type. It should from [text, url, image/png, image/jpeg]")

  }
}
