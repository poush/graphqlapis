import { Table, Unique, Column, Model, HasMany, Default, BelongsToMany,  PrimaryKey, AutoIncrement, ForeignKey, BeforeSave } from 'sequelize-typescript';
import { userChannel } from "./userChannel.model";
import { Channel } from "./channel.model";
import * as bcrypt from 'bcrypt';
import * as crypto from 'crypto';
import to from 'await-to-js';
import * as jsonwebtoken from 'jsonwebtoken';
import Email from '../services/mail';
import TEMPLATES from '../config/templates';
import { ENV } from '../config';


@Table({timestamps: true})
export class User extends Model<User> {
  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @Unique
  @Column
  email: string;

  @Column
  password: string;

  @BelongsToMany(() => Channel, () => userChannel)
  channels: Channel[];

  @Column
  emailToken: string;

  @Column
  resetToken: string;

  @Default(false)
  @Column
  isVerified: boolean;

  @BeforeSave
  static async hashPassword(user: User) {
    let err;
    if (user.changed('password')){
        let salt, hash;
        [err, salt] = await to(bcrypt.genSalt(10));
        if(err) {
          throw err;
        }

        [err, hash] = await to(bcrypt.hash(user.password, salt));
        if(err) {
          throw err;
        }
        user.password = hash;
    }
  }

  async comparePassword(pw) {
      let err, pass;
      if(!this.password) {
        throw new Error('Does not have password');
      }

      [err, pass] = await to(bcrypt.compare(pw, this.password));
      if(err) {
        throw err;
      }

      if(!pass) {
        throw 'Invalid password';
      }

      return this;
  };

  getJwt(){
      return 'Bearer ' + jsonwebtoken.sign({
          id: this.id,
      }, ENV.JWT_ENCRYPTION, { expiresIn: ENV.JWT_EXPIRATION });
  }

  async sendActivation() {
    let email = new Email();
    let token = await crypto.randomBytes(64).toString('hex');
    
    this.emailToken = token;
    this.save();
    
    email.set(this.email, 'Activation Token', TEMPLATES.ACTIVATION(token)).send()
  }

  async resetPasswordToken() {
    let email = new Email();
    let token = await crypto.randomBytes(64).toString('hex');
    
    this.resetToken = token;
    this.save();
    
    email.set(this.email, 'Password Reset Token', TEMPLATES.FORGOT(token)).send()
  }

  public static async verifyToken(email: string, token: string) {
    let user = await User.findOne({
      where: {
        email: email,
        emailToken: token,
        isVerified: false
      }
    })

    if(user){
      user.isVerified = true;
      user.emailToken = null;
      await user.save();
      return true;
    }

    return false;
  }
}
