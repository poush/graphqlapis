import { Table, Column, Model, HasMany, BelongsToMany, ForeignKey, PrimaryKey, HasOne, AutoIncrement } from 'sequelize-typescript';
import { User } from './user.model'
import { Message } from './message.model'
import { userChannel } from './userChannel.model'
@Table({timestamps: true})

export class Channel extends Model<Channel> {

  @PrimaryKey
  @AutoIncrement
  @Column
  id: number;

  @Column
  name: string;

  @BelongsToMany(() => User, () => userChannel)
  users: User[];

  @HasMany(() => Message)
  messages: Message[];

}
