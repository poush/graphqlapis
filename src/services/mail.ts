import sg = require("@sendgrid/mail");
import { ENV } from '../config';


export default class Email {

	public static FROM: string = "no-reply@ipiyush.com";
	public static FROM_NAME: string = "Piyush Agrawal";

	public SUBJECT: string;
	public HTML: string;
	public TO: string;

	protected _mailer: any;

	constructor() {
		this._mailer = sg;
		this._mailer.setApiKey(ENV.SENDGRID_API);
	}

	set(to: string, subject: string, html: string) {
		this.TO = to;
		this.SUBJECT = subject;
		this.HTML = html;
		return this;
	}

	getContent() {
		return {
			to: this.TO,
			from: Email.FROM,
			subject: this.SUBJECT,
			html: this.HTML
		};
	}

	async send() {
		let content = this.getContent();
		let error = await this._mailer.send(content);

		if(error)
			throw error;
	}
}