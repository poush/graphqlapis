export const Validator = {

	'image': (text) => {
		let regex = /^\s*data:([a-z]+\/[a-z]+(;[a-z\-]+\=[a-z\-]+)?)?(;base64)?,[a-z0-9\!\$\&\'\,\(\)\*\+\,\;\=\-\.\_\~\:\@\/\?\%\s]*\s*$/i;
		return !!text.match(regex);
	},

	'text': (text) => {
		return [true, null];
	},

	'image/png': (text) => {
		return [Validator['image'](text), 'Image data url expected']
	},

	'image/jpeg': (text) => {
		return Validator['image/png'](text);
	},

	url: (text) => {
		let regex = new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
		if(regex.test(text))
			return [true, null];
		return [false, 'Invalid Url'];
	}
}