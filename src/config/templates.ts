import {ENV} from '../config';

export default {
	'FORGOT': (token: string) => {
		return `Use this token ${token} to create new password.`
	},

	'ACTIVATION': (token: string) => {
		return `Activation token: ${token}`
	}
}