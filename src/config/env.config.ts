import * as dotEnv from 'dotenv';
dotEnv.config();

export const ENV = {
    PORT: process.env.PORT || '3000',

    DB_HOST: process.env.DB_HOST || 'us-cdbr-iron-east-01.cleardb.net',
    DB_PORT: process.env.DB_PORT || '3306',
    DB_NAME: process.env.DB_NAME || 'heroku_c0448bf07a5b556',
    DB_USER: process.env.DB_USER || 'b2aff8c42ecf3d',
    DB_PASSWORD: process.env.DB_PASSWORD || '',
    DB_DIALECT: process.env.DB_DIALECT || 'mysql',

    JWT_ENCRYPTION: process.env.JWT_ENCRYPTION || 'secureKey',
    JWT_EXPIRATION: process.env.JWT_EXPIRATION || '1y',
    SENDGRID_API: process.env.SENDGRID_API,
    SITE_URL: process.env.SITE_URL,
};
