# swapcard


`npm install`
`npm run start`


### Tasks

- [x] User creation
- [x] Authentication with session or stateless session
- [x] You must know which user sent a specific message
- [x] Create a channel
- [x] List the channels
- [x] List the messages inside a channel
- [x] Send a message to a channel
- [x] Use a database (MongoDB/MySQL/PostgresQL/Cassandra or other)
- [ ] A ReactJS client. The client doesn’t need to implement every API feature but should be functional.
- [x] Update/delete user (delete left)
- [x] Email verification
- [x] Password recovery
- [x] Delete a channel (doesn't delete all its messages right now)
- [x] A channel owner can invite/kick another user into to/from his channel
- [x] Users cannot read nor write into channel they aren’t a member of
- [x] Update/delete messages
- [x] Add image/file to a message
- [x] Url metadata enrichment (this should be done on client side)
- [ ] Message advanced search
- [ ] Api unit testing
- [x] Try to keep it simple
- [x] All the included features should work as intended
- [x] Use modern JavaScript
- [x] Using a strict syntactical superset of JavaScript like TypeScript or Flow is appreciated
- [x] Using GraphQL is very appreciated,
